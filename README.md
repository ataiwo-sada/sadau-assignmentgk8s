# SADAU-AssignmentGK8s


## **Homework Part 1: Google Kubernetes Engine**

In this assignment we focus on exploring Google's managed Kubernetes offering.

**Requirements**

**Description**

**Success criteria**

**Artifacts to be provided**

**Bonus points**

## **Requirements**

A Linux/OSX environment
Running windows? Spin up a virtual machine with VirtualBox. CentOS or Ubuntu are easy enough and will get the job done.
A free GCP account
Description

Acquire a Google account. Google Cloud has a free trial where they provide you with $300 in credits for a year. Use these credits to:

Spin up a Kubernetes cluster in GKE with two-three nodes
Write a Kubernetes deployment definition in YAML that includes:
an Nginx web server with a single hello-world webpage
Write a Kubernetes service definition in YAML that exposes the deployment with a Load Balancer
Write a Kubernetes ingress definition to allow the service to be reached from the world
## **Success criteria**

The webpage can be displayed over the internet: there is an endpoint I can see the page on.


## **Artifacts to be provided**

Resulting publicly accessible URL
Kubernetes Deployment, Service, Ingress YAMLs
nginx configuration


## **Bonus points**

Force HTTPS redirection with Nginx (using HTTP 301)
Configure DNS to the endpoint
Ensure certificate validity for the endpoint
Define all infrastructure using IaC - Infrastructure as Code, such as Terraform or Deployment Manager
Define all Kubernetes objects using a templating engine of choice (e.g., Helm or Kustomize)



## **Solution to the Assignment defined all Kubernetes objects using a templating engine Helm3**

Helm (https://helm.sh) is a package manager for Kubernetes. The three main concepts in Helm are:

**Chart:** is a Helm package that contains all of the resource definitions necessary to run an application, tool, or service inside of a Kubernetes cluster.

**Repository:** where the charts can be collected and shared.

**Release:** is an instance of a chart running in a Kubernetes cluster.



**Step 1:** Download the current binary release: using this command below

*wget https://get.helm.sh/helm-v3.0.1-linux-amd64.tar.gz
tar -zxvf helm-v3.0.1-linux-amd64.tar.gz
sudo mv linux-amd64/helm /usr/local/bin/helm3*

**Step 2:** Check the version with “helm3 version”

**Step 3:** Setup the official repository: using this command below

*helm repo add stable https://charts.helm.sh/stable*

**Step 4:** Create Kubernetes Cluster:

*gcloud container clusters create gke-test1 --project "mytestk8app" --zone "us-central1-c" --cluster-version "1.16.15-gke.4901" --machine-type "n1-standard-1" --num-nodes "2" --enable-autoscaling --min-nodes "2" --max-nodes "3""*

**Step 5:** Wait until the cluster is up and running and then download the cluster credentials:

*gcloud container clusters get-credentials gke-test1 --zone us-central1-c --project mytestk8app*

**Step 6:** After that, check that we can connect to our new Kubernetes cluster:

*kubectl get nodes*

**Step 7:** Setup the Nginx Ingress Controller by first creating Nginx’s namespace:

*kubectl create ns nginx*

**Step 8:** Then execute the Nginx’s chart in:

*helm3 install nginx stable/nginx-ingress --namespace nginx --set rbac.create=true --set controller.publishService.enabled=true*

**Step 9:** And wait until you can see the external IP assigned to the Nginx Ingress controller:

*kubectl get svc -n nginx*

The controller is serving on ports 80 and 443 and the required firewall rules were setup (“VPC Networks” > “Firewall rules”):

**Step 10:** To deploy the hello world application to validate the ingress controller. Create a deployment:

*kubectl create deployment hello-app --image=gcr.io/google-samples/hello-app:1.0*

**Step 11:** And expose it (create our service)

*kubectl expose deployment hello-app --port 8080 --target-port 8080*

**Step 12:** Create an Ingress resource (hello-app-ingress.yaml)

**Step 13:** And deploy the ingress controller:

*kubectl apply -f hello-app-ingress.yaml*

**Step 14:** To access the url go to “Frontends” column in the GCP Console (“Kubernetes Engine” > “Services & Ingress” and go to “Ingresses” tab):  http://35.239.88.164/helloworld





